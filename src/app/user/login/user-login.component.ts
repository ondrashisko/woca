import { Location } from '@angular/common';
import { UserMenuComponent } from '../user-menu/user-menu.component';
import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserService, RegisterUserInterface } from '../../services/user.service';
import jwt_decode from 'jwt-decode';
import { WocaService } from 'src/app/services/woca.service';

@Component({
  selector: 'app-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.sass'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup
  signupForm: FormGroup
  passIncorect: boolean;
  signupActive: boolean;
  isLoggedIn: boolean

  constructor(
    private _us: UserService,
    private _ws: WocaService,
    private router: Router,
    private fb: FormBuilder,
  ){
    this.loginForm = this.fb.group({
      email: ['',[Validators.email, Validators.required]],
      password: ['',[Validators.required]]});
    this.signupForm =  this.fb.group({
      username: ['', [Validators.required]],
      email: ['',[Validators.email, Validators.required]],
      password: ['',[Validators.required]]
    });

  }


  ngOnInit() {
    this.isLoggedIn = this._us.isLoggedin
  }

  // convenience getter for easy access to form fields
  get fl() { return this.loginForm.controls; }
  get fs() { return this.signupForm.controls; }

  login(email: string , password: string){
    this._us.login(email,password).subscribe(
      data=>{
        localStorage.setItem('token',data['token'])
        this._us._user.next(data['payload'].user)
        this._ws.language = data['payload'].user.choosenLanguage
        this.router.navigate(['in/edit']);
      },
      err=> {
        this.passIncorect = true
        console.log('login err',err);
      }
    )}

  signup(){
      let sfv : RegisterUserInterface = this.signupForm.value
      this._us.signup(sfv).subscribe(
        data=>console.log(data),
        err=>console.log(err)
      )
    }

}

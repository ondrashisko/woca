import { ActivatedRoute, Router } from '@angular/router';
import { LoginComponent } from '../login/user-login.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UserService } from '../../services/user.service';
import { User } from '../User';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.sass']
})
export class UserMenuComponent implements OnInit {
  user: User

  constructor(
    public _us: UserService,
    private dialog: MatDialog,
    private router:Router
  ) {

      }

  ngOnInit() {
    this.user = this._us.user
    this._us._user.subscribe(data=>{
      this.user = data
    })

  }


  login(){
    const dialogRef = this.dialog.open(LoginComponent, {
      width: '400px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed',result);
    });
  }

  logout(){
   this._us.logout()
  }

}

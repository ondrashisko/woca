export class User{
    _id: string;
    username:string;
    email: string;
    createdAt: Date;
    lang: string
    languages: string[]
    choosenLanguage: string

    constructor(){

    }
}

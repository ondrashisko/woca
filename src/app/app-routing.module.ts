import { LoginComponent } from './user/login/user-login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingComponent } from './components/landing/landing.component';
import { ProfileComponent } from './components/profile/profile.component';
import { EditComponent } from './components/edit/edit.component';
import { LearnComponent } from './components/learn/learn.component';
import { AuthGuard } from './user/auth.guard';
import { InComponent } from './components/in/in.component';


const routes: Routes = [
  {
    path: "",
    component: LandingComponent
  },
  {
    path:"landing",
    redirectTo:""
  },
  {
    path:'in',
    component: InComponent,
    canActivate: [AuthGuard],
    children:[
      {
        path: 'edit',
        component: EditComponent,
      },
      {
        path: 'learn',
        component: LearnComponent,
      },
      {
        path: 'profile',
        component: ProfileComponent,
      }
    ]
  },
  {
    path:'**',
    component: LandingComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Woc } from '../Woc';
import { getHeaders, UserService } from './user.service';
import { Observable, Observer, of, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

const URL = environment.apiURL + '/api/wocs'

@Injectable({
  providedIn: 'root'
})
export class WocaService {
  public language: string

  constructor(
    private http: HttpClient,
    private _us: UserService
  ) {
    this.language = localStorage.getItem('language')
  }

  get headers(){
    return {headers: getHeaders()}
  }

  getWocs(lang: string) {
    return this.http.get(URL+'/'+lang,this.headers)
  }
  getWocsToLearn(lang: string){
    return this.http.get(URL+'/wocsToLearn/'+lang,this.headers)
  }
  getWocsByKnownNumber(lang: string, knownNumber: number) {
    let body={ lang: lang, knownNumber:knownNumber}
    return this.http.post(URL+'/WocsByKnownNumber',body,this.headers)
  }
  createWoc(w: Woc) {
    return this.http.post(URL,w,this.headers)
  }

  updateWoc(w: Woc) {
    return this.http.put(URL +'/'+ w._id, w, this.headers)
  }

  deleteWoc(_id) {
    return this.http.delete(URL +'/'+ _id,this.headers)
  }

  setLanguage(l:string){
    this.language = l
    localStorage.setItem('language',l)
  }
}

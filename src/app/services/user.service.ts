import { User } from './../user/User';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import jwt_decode from 'jwt-decode';
import { of, Subject } from 'rxjs';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

const URL = environment.apiURL+'/user/'

@Injectable({
  providedIn: 'root'
})
export class UserService {
  _user: Subject<User> = new Subject<User>()

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
      let token =  localStorage.getItem('token')
      if (token){
        let d_token = jwt_decode(token)
        this._user.next(d_token.user)
      }else{
        this._user.next(undefined)
      }
      this.me.subscribe(
        data => this._user.next(data as User)
      )
  }

  signup(u: RegisterUserInterface){
    return this.http.post(URL+'signup',u)
  }

  login(email: string, password: string){
    let u = {email: email, password: password}
    return this.http.post(URL+'login',u)
  }

  logout(){
    localStorage.removeItem('token');
    localStorage.removeItem('language');
    this._user.next(undefined);
    this.router.navigate(["/login"]);
  }

  public get me(){
    let token = localStorage.getItem('token')
    if (this.isLoggedin) {
    let headers = getHeaders()
    return this.http.get(URL +'me',{headers:headers})
    }else{
      return of(undefined)
    }
  }

  public get isLoggedin(){
    let token =  localStorage.getItem('token')
    if (!token)
      return false
    else {
      let d_token = jwt_decode(token)
      if (d_token.user) return true
    }
  }

  get user(){
    let token =  localStorage.getItem('token')
    if (!token)
      return undefined
    else {
      let d_token = jwt_decode(token)
      if (d_token.user) return d_token.user
    }
  }

  updateLanguages(langs: string[]){
    return this.http.put(URL+ 'languages',langs,{headers: getHeaders()})
  }

  setLang(lang: string){
    localStorage.setItem('language',lang)
    return this.http.put(URL+ 'change-language',{lang:lang},{headers: getHeaders()})
  }


}

export interface RegisterUserInterface{
  username: string,
  email: string,
  password: string
}

export function getHeaders(){
  return new HttpHeaders({
    "Authorization": localStorage.getItem("token"),
  })
}

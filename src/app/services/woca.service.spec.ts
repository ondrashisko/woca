import { TestBed } from '@angular/core/testing';

import { WocaService } from './woca.service';

describe('WocaService', () => {
  let service: WocaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WocaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

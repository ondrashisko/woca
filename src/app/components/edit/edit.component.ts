import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/user/User';
import { Woc } from 'src/app/Woc';
import { WocaService } from '../../services/woca.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {
  wocs: Woc[]
  language: string

  constructor(
    private _ws: WocaService,
    private _us: UserService
  ) { }

  ngOnInit(): void {
    this.language = this._ws.language
    this._us._user.subscribe(data=>{
      if (data) this.getWocs(this._ws.language)
    })
    this.getWocs(this._ws.language)
  }

  getWocs(lang: string){
    this._ws.getWocs(lang).subscribe(
      data=>{
        this.wocs = data as Woc[]
      },
      err=>console.log(err)
    )
  }

  addWoc(e){
    this.wocs.push(e)    
  }

  deleteWoc(_id: string,i: number){
    this._ws.deleteWoc(_id).subscribe(
      data=> this.wocs.splice(i,1)
    )
  }
}

import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Woc } from 'src/app/Woc';
import { WocaService } from '../../services/woca.service';

@Component({
  selector: 'app-learn',
  templateUrl: './learn.component.html',
  styleUrls: ['./learn.component.sass']
})
export class LearnComponent implements OnInit {
  wocs: Woc[]=[]
  language: string
  i: number =0
  turn = false
  loading = true

  constructor(
    private _us: UserService,
    private _ws: WocaService
  ) { }

  ngOnInit(): void {
    this.language = this._ws.language
    this._us._user.subscribe(data=>{
      if (data) this.getWocs(data.choosenLanguage)
    })
    if (this.loading) this.getWocs(this.language)
  }


  getWocs(lang: string){
    this._ws.getWocsToLearn(lang).subscribe(data=>{
      this.wocs=this.shufleArray(data as Woc[])
      this.loading = false
    })
  }

  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  shufleArray(a:Woc[]){
    let sa:any[]=[] //shufled array
    let l = a.length
    let i = 0
    while (i<l){
      let r = this.getRandomInt(l)
      if (!sa.includes(a[r])) {
        sa.push(a[r])
        i++
      } 
    }
    return sa
  }
  

  showNext(){
    if (this.i < this.wocs.length-1){
      this.i++
    }else{
      this.i = 0
    }
  }

  turnCard(){
    if (!this.turn)
      this.turn = !this.turn
    else{
      this.turn = !this.turn
      this.showNext()
    }
  }

  known(mark: string){
    let w = this.wocs[this.i]
    if (mark == '+' && w.known<3) w.known++
    if (mark == '-' && w.known >0) w.known--
    this._ws.updateWoc(w).subscribe()
  }

}

import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { WocaService } from 'src/app/services/woca.service';
import { User } from 'src/app/user/User';

@Component({
  selector: 'app-languages',
  templateUrl: './languages.component.html',
  styleUrls: ['./languages.component.sass']
})
export class LanguagesComponent implements OnInit {
  isLoggedin:boolean
  user: User

  constructor(
    private _ws: WocaService,
    private _us: UserService,
  ) {

  }

  ngOnInit(): void {
    this.user = this._us.user
    this._us._user.subscribe(data=>{
      this.user= data
    })
  }

  setLanguage(l:string){
    this.user.choosenLanguage = l
    this._ws.setLanguage(l);
    this._us._user.next(this.user)
    this._us.setLang(l).subscribe()
  }
}

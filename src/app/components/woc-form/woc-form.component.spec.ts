import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WocFormComponent } from './woc-form.component';

describe('WocFormComponent', () => {
  let component: WocFormComponent;
  let fixture: ComponentFixture<WocFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WocFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WocFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Woc } from './../../Woc';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { WocaService } from '../../services/woca.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-woc-form',
  templateUrl: './woc-form.component.html',
  styleUrls: ['./woc-form.component.sass']
})
export class WocFormComponent implements OnInit {
  woc = new Woc()
  @Output() created= new EventEmitter()

  constructor(
    private _ws: WocaService,
    private _us : UserService
  ) { }

  ngOnInit(): void {
  }

  onSubmit(wocForm: any) {
    this.woc.known = 0
    this.woc.date = new Date()
    this.woc.language = this._ws.language
    this._ws.createWoc(this.woc).subscribe(
      data=>{
        this.created.emit(data)
        wocForm.form.reset()
      }
    )
  }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { getMaxListeners } from 'process';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.sass']
})
export class LandingComponent implements OnInit {

  constructor(
    private _us: UserService,
    private router: Router
  ) { }

  ngOnInit(): void {
    if (this._us.isLoggedin){
    this.router.navigate(['/in/edit']);
    }
  }

}

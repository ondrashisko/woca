import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { WocaService } from 'src/app/services/woca.service';
import { User } from 'src/app/user/User';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass'],
})
export class ProfileComponent implements OnInit {
  user: User;
  constructor(private _us: UserService, private _ws: WocaService) {}

  ngOnInit(): void {
    this._us.me.subscribe((data) => (this.user = data as User));
  }

  updateLangs() {
    this._us.updateLanguages(this.user.languages).subscribe();
    this._us._user.next(this.user)
    this._us.setLang(this.user.choosenLanguage).subscribe()
    this._ws.setLanguage(this.user.choosenLanguage);
  }
  addLang(langInput) {
    this.user.languages.push(langInput.value);
    this.user.choosenLanguage = langInput.value
    this.updateLangs(); 
  }

  removeLang(i) {
    if (this.user.languages[i] == this.user.choosenLanguage){
      this.user.choosenLanguage = undefined;
    }
    this.user.languages.splice(i, 1);
    if (this.user.languages[i - 1]){
      this.user.choosenLanguage = this.user.languages[i - 1]
    }
    else {
      // removed last lang
      this.user.choosenLanguage = undefined
    }

    this.updateLangs();
  }
}

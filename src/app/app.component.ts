import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'woca';
  localesList = [
    { code: 'en-US', label: 'English' },
    { code: 'cs-CS', label: 'Česky' }
  ]
}

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.wocRoutes = void 0;
var express = require("express");
var auth_1 = require("../middleware/auth");
var Woc_schema_1 = require("../models/Woc.schema");
var wocRoutes = express.Router();
exports.wocRoutes = wocRoutes;
wocRoutes.get('/', auth_1.auth, function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var uid, items, err_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                uid = req['user']._id;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 4, , 5]);
                return [4 /*yield*/, Woc_schema_1.WocModel.find({ uid: uid })];
            case 2: return [4 /*yield*/, (_a.sent())];
            case 3:
                items = _a.sent();
                res.status(200);
                res.json(items);
                res.end();
                return [3 /*break*/, 5];
            case 4:
                err_1 = _a.sent();
                res.status(500);
                res.end();
                console.error(err_1);
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); });
wocRoutes.get('/:lang', auth_1.auth, function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var uid, lang, items, err_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                uid = req['user']._id;
                lang = req.params.lang;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 4, , 5]);
                return [4 /*yield*/, Woc_schema_1.WocModel.find({ uid: uid, language: lang })];
            case 2: return [4 /*yield*/, (_a.sent())];
            case 3:
                items = _a.sent();
                res.status(200);
                res.json(items);
                res.end();
                return [3 /*break*/, 5];
            case 4:
                err_2 = _a.sent();
                res.status(500);
                res.end();
                console.error(err_2);
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); });
wocRoutes.post('/', auth_1.auth, function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var w, uid, item;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                w = req.body;
                uid = req['user']._id;
                w.uid = uid;
                console.log(req['user']);
                item = new Woc_schema_1.WocModel(w);
                console.log(item);
                return [4 /*yield*/, item.save()];
            case 1:
                _a.sent();
                res.json(item);
                res.end();
                return [2 /*return*/];
        }
    });
}); });
wocRoutes.put("/:id", auth_1.auth, function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var w, wn;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                w = req.body;
                wn = {
                    cz: w.cz,
                    tran: w.tran,
                    language: w.language,
                    known: w.known,
                    date: w.date,
                    uid: w.uid
                };
                if (!(wn['uid'] == req['user']._id)) return [3 /*break*/, 2];
                return [4 /*yield*/, Woc_schema_1.WocModel.findOneAndUpdate({ _id: req.params.id }, wn)];
            case 1:
                _a.sent();
                return [3 /*break*/, 3];
            case 2:
                res.status(401);
                _a.label = 3;
            case 3:
                res.end();
                return [2 /*return*/];
        }
    });
}); });
wocRoutes.delete("/:id", auth_1.auth, function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var wid, w;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                wid = req.params.id;
                return [4 /*yield*/, Woc_schema_1.WocModel.find({ _id: wid })];
            case 1:
                w = _a.sent();
                if (!(w[0]['uid'] == req['user']._id)) return [3 /*break*/, 3];
                return [4 /*yield*/, Woc_schema_1.WocModel.findByIdAndRemove(wid)];
            case 2:
                _a.sent();
                res.status(200);
                return [3 /*break*/, 4];
            case 3:
                res.status(401);
                _a.label = 4;
            case 4:
                res.end();
                return [2 /*return*/];
        }
    });
}); });
// WOCS BY KNOWN NUMBER (WocsByKnownNumber)
wocRoutes.post('/WocsByKnownNumber', auth_1.auth, function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var lang, knownNumber, uid, items, err_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                lang = req.body.lang;
                knownNumber = req.body.knownNumber;
                uid = req['user']._id;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 4, , 5]);
                return [4 /*yield*/, Woc_schema_1.WocModel.find({ uid: uid, language: lang, known: knownNumber })];
            case 2: return [4 /*yield*/, (_a.sent())];
            case 3:
                items = _a.sent();
                res.status(200);
                res.json(items);
                res.end();
                return [3 /*break*/, 5];
            case 4:
                err_3 = _a.sent();
                res.status(500);
                res.end();
                console.error(err_3);
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); });
// wocs to learn /wocsToLearn/
wocRoutes.get('/wocsToLearn/:lang', auth_1.auth, function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var uid, lang, items, wocs, err_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                uid = req['user']._id;
                lang = req.params.lang;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 4, , 5]);
                return [4 /*yield*/, Woc_schema_1.WocModel.find({ uid: uid, language: lang })];
            case 2: return [4 /*yield*/, (_a.sent())];
            case 3:
                items = _a.sent();
                wocs = sortItemsToLearn(items);
                console.log(wocs.length);
                res.status(200);
                res.json(wocs);
                res.end();
                return [3 /*break*/, 5];
            case 4:
                err_4 = _a.sent();
                res.status(500);
                res.end();
                console.error(err_4);
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); });
function sortItemsToLearn(w) {
    var wocs = [];
    for (var i = 2; i >= 0; i--) {
        switch (i) {
            case 0: {
                var missing = 10 - wocs.length;
                wocs.push.apply(wocs, addXWocs(w, i, missing));
                break;
            }
            case 1: {
                if (w.length >= 3) {
                    wocs.push.apply(wocs, addXWocs(w, i, 3));
                }
                break;
            }
            case 2: {
                if (w.length >= 2) {
                    wocs = wocs.concat(addXWocs(w, i, 2));
                }
                break;
            }
            default: {
                break;
            }
        }
    }
    return wocs;
}
function addXWocs(w, k, n) {
    var i = 0;
    var ws = [];
    w = w.filter(function (a) { return a.known == k; });
    var l = w.length;
    console.log('n', n, 'l', l, 'k', k, 'w', w);
    if (n > l)
        return w;
    while (i < n) {
        var r = getRandomInt(l);
        if (!ws.includes(w[r])) {
            ws.push(w[r]);
            i++;
        }
    }
    return ws;
}
function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

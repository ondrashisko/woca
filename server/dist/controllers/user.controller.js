"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.userRouter = void 0;
var express = require('express');
var _a = require('express-validator'), check = _a.check, validationResult = _a.validationResult;
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
var userRouter = express.Router();
exports.userRouter = userRouter;
var auth_1 = require("../middleware/auth");
var User_schema_1 = require("../models/User.schema");
/**
 * @method - POST
 * @param - /signup
 * @description - User SignUp
 */
userRouter.post('/signup', [
    check('username', 'Please Enter a Valid Username').not().isEmpty(),
    check('email', 'Please enter a valid email').isEmail(),
    check('password', 'Please enter a valid password').isLength({
        min: 6,
    }),
], function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var errors, _a, username, email, password, user, salt, _b, payload, err_1;
    return __generator(this, function (_c) {
        switch (_c.label) {
            case 0:
                errors = validationResult(req);
                if (!errors.isEmpty()) {
                    return [2 /*return*/, res.status(400).json({
                            errors: errors.array(),
                        })];
                }
                _a = req.body, username = _a.username, email = _a.email, password = _a.password;
                _c.label = 1;
            case 1:
                _c.trys.push([1, 6, , 7]);
                return [4 /*yield*/, User_schema_1.UserModule.findOne({
                        email: email,
                    })];
            case 2:
                user = _c.sent();
                if (user) {
                    return [2 /*return*/, res.status(400).json({
                            msg: 'User Already Exists',
                        })];
                }
                user = new User_schema_1.UserModule({
                    username: username,
                    email: email,
                    password: password,
                });
                return [4 /*yield*/, bcrypt.genSalt(10)];
            case 3:
                salt = _c.sent();
                _b = user;
                return [4 /*yield*/, bcrypt.hash(password, salt)];
            case 4:
                _b.password = _c.sent();
                return [4 /*yield*/, user.save()];
            case 5:
                _c.sent();
                payload = {
                    user: {
                        id: user.id,
                    },
                };
                jwt.sign(payload, 'randomString', {
                    expiresIn: 10000,
                }, function (err, token) {
                    if (err)
                        throw err;
                    res.status(200).json({
                        token: token,
                    });
                });
                return [3 /*break*/, 7];
            case 6:
                err_1 = _c.sent();
                console.log(err_1.message);
                res.status(500).send('Error in Saving');
                return [3 /*break*/, 7];
            case 7: return [2 /*return*/];
        }
    });
}); });
userRouter.post('/login', [
    check('email', 'Please enter a valid email').isEmail(),
    check('password', 'Please enter a valid password').isLength({
        min: 6,
    }),
], function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var errors, _a, email, password, user, isMatch, payload_1, e_1;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                errors = validationResult(req);
                if (!errors.isEmpty()) {
                    return [2 /*return*/, res.status(400).json({
                            errors: errors.array(),
                        })];
                }
                _a = req.body, email = _a.email, password = _a.password;
                _b.label = 1;
            case 1:
                _b.trys.push([1, 4, , 5]);
                return [4 /*yield*/, User_schema_1.UserModule.findOne({
                        email: email,
                    })];
            case 2:
                user = _b.sent();
                if (!user)
                    return [2 /*return*/, res.status(400).json({
                            message: 'User Not Exist',
                        })];
                return [4 /*yield*/, bcrypt.compare(password, user.password)];
            case 3:
                isMatch = _b.sent();
                if (!isMatch)
                    return [2 /*return*/, res.status(400).json({
                            message: 'Incorrect Password !',
                        })];
                user.password = '';
                payload_1 = {
                    user: user,
                };
                jwt.sign(payload_1, 'randomString', {
                    expiresIn: 518400,
                }, function (err, token) {
                    if (err)
                        throw err;
                    res.status(200).json({
                        token: token,
                        payload: payload_1,
                    });
                });
                return [3 /*break*/, 5];
            case 4:
                e_1 = _b.sent();
                console.error(e_1);
                res.status(500).json({
                    message: 'Server Error',
                });
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); });
/**
 * @method - POST
 * @description - Get LoggedIn User
 * @param - /user/me
 */
userRouter.get('/me', auth_1.auth, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var user, e_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, User_schema_1.UserModule.findById(req.user._id)];
            case 1:
                user = _a.sent();
                res.json(user);
                return [3 /*break*/, 3];
            case 2:
                e_2 = _a.sent();
                res.send({ message: 'Error in Fetching user' });
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
/**
 * @method - PUT
 * @description - adds learning language
 * @param - /user/languages
 */
userRouter.put('/languages', auth_1.auth, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var langs, _id, user, err_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                langs = req.body;
                _id = req.user._id;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 4, , 5]);
                return [4 /*yield*/, User_schema_1.UserModule.findById(_id)];
            case 2:
                user = _a.sent();
                user.languages = langs;
                return [4 /*yield*/, User_schema_1.UserModule.findByIdAndUpdate({ _id: _id }, user)];
            case 3:
                _a.sent();
                res.status(200);
                return [3 /*break*/, 5];
            case 4:
                err_2 = _a.sent();
                console.error(err_2);
                res.status(500).send('error in updating users language');
                return [3 /*break*/, 5];
            case 5:
                res.end();
                return [2 /*return*/];
        }
    });
}); });
/**
 * @method - PUT
 * @description - change learning language
 * @param - /user/change-language
 */
userRouter.put('/change-language', auth_1.auth, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var lang, _id, user, err_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                lang = req.body.lang;
                console.log('lang', lang);
                _id = req.user._id;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 4, , 5]);
                return [4 /*yield*/, User_schema_1.UserModule.findById(_id)];
            case 2:
                user = _a.sent();
                if (lang) {
                    user.choosenLanguage = lang;
                }
                else {
                    user.choosenLanguage = null;
                }
                console.log(user);
                return [4 /*yield*/, User_schema_1.UserModule.findByIdAndUpdate({ _id: _id }, user)];
            case 3:
                _a.sent();
                res.status(200);
                return [3 /*break*/, 5];
            case 4:
                err_3 = _a.sent();
                console.error(err_3);
                res.status(500).send('error updating USERS chosen language');
                return [3 /*break*/, 5];
            case 5:
                res.end();
                return [2 /*return*/];
        }
    });
}); });

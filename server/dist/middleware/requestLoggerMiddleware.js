"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.requestLoggerMiddleware = void 0;
var requestLoggerMiddleware = function (req, res, next) {
    console.info(req.method + " " + req.originalUrl);
    var start = new Date().getTime();
    res.on("finish", function () {
        var ellapsed = new Date().getTime() - start;
        console.info(req.method + " " + req.originalUrl + " -> " + ellapsed + "ms");
    });
    next();
};
exports.requestLoggerMiddleware = requestLoggerMiddleware;

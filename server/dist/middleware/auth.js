"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.auth = void 0;
var jwt = require("jsonwebtoken");
var auth = function (req, res, next) {
    var token = req.headers.authorization;
    if (!token)
        return res.status(401).json({ message: "Auth Error token not presented in heder" });
    try {
        var decoded = jwt.verify(token, "randomString");
        req.user = decoded.user;
        next();
    }
    catch (e) {
        console.error(e);
        res.status(500).send({ message: "Invalid Token" });
    }
};
exports.auth = auth;

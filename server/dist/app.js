"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.app = void 0;
var express = require("express");
var cors = require("cors");
var bodyparser = require("body-parser");
var requestLoggerMiddleware_1 = require("./middleware/requestLoggerMiddleware");
var woc_controller_1 = require("./controllers/woc.controller");
var user_controller_1 = require("./controllers/user.controller");
var app = express();
exports.app = app;
app.use(cors());
app.use(bodyparser.json());
// middleware
app.use(requestLoggerMiddleware_1.requestLoggerMiddleware);
// Routes
app.use("/api/wocs", woc_controller_1.wocRoutes);
app.use("/user", user_controller_1.userRouter);
app.get("/", function (req, res) {
    res.send("wocs express server running");
    res.end();
});

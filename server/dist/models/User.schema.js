"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserModule = void 0;
var mongoose = require("mongoose");
var UserSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    lang: {
        type: String,
        default: 'cs'
    },
    languages: {
        type: Array
    },
    choosenLanguage: {
        type: String,
    }
});
// export model user with UserSchema
var UserModule = mongoose.model("User", UserSchema);
exports.UserModule = UserModule;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WocModel = void 0;
var mongoose = require("mongoose");
var WocSchema = new mongoose.Schema({
    cz: { type: String, required: true },
    tran: { type: String, required: true },
    language: { type: String, required: true },
    known: Number,
    date: { type: Date },
    uid: { type: String, required: true }
});
var WocModel = mongoose.model('Woc', WocSchema);
exports.WocModel = WocModel;

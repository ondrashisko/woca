import { Response, Request, NextFunction } from 'express'

const requestLoggerMiddleware = (req: Request, res: Response, next: NextFunction) => {
    console.info(`${req.method} ${req.originalUrl}`);
    const start = new Date().getTime()
    res.on("finish",()=>{
        const ellapsed = new Date().getTime() - start
        console.info(`${req.method} ${req.originalUrl} -> ${ellapsed}ms`)
    })
    next()
}

export { requestLoggerMiddleware }
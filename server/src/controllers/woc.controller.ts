import *  as express  from 'express'
import { NextFunction, Request,Response} from 'express'
import * as mongodb from 'mongodb';
import { auth } from '../middleware/auth';
import { WocModel, WocInterface } from '../models/Woc.schema';

const wocRoutes = express.Router()

wocRoutes.get('/',auth,async(req:Request,res:Response, next:NextFunction)=>{
  let uid = req['user']._id
      try{
        let items =  await (await WocModel.find({uid}))
        res.status(200)
        res.json(items)
        res.end()
    }catch(err){
        res.status(500);
        res.end()
        console.error(err)
    }
})

wocRoutes.get('/:lang',auth,async(req:Request,res:Response, next:NextFunction)=>{
  let uid = req['user']._id
  let lang = req.params.lang
      try{
        let items =  await (await WocModel.find({uid,language:lang}))
        res.status(200)
        res.json(items)
        res.end()
    }catch(err){
        res.status(500);
        res.end()
        console.error(err)
    }
})

wocRoutes.post('/',auth,async (req:Request,res:Response, next:express.NextFunction)=>{
    const w = req.body
    const uid = req['user']._id
    w.uid= uid
    console.log(req['user']);
    const item = new WocModel(w)
    console.log(item);
    await item.save()
    res.json(item)
    res.end()
})

wocRoutes.put("/:id",auth,async(req:Request,res:Response, next:express.NextFunction)=>{
    let w = req.body
    let wn : WocInterface = {
        cz:w.cz,
        tran:w.tran,
        language: w.language,
        known: w.known,
        date: w.date,
        uid: w.uid
    }
    if (wn['uid']==req['user']._id)
      await WocModel.findOneAndUpdate({_id:req.params.id},wn)
    else
      res.status(401)
    res.end()
})

wocRoutes.delete("/:id",auth,async (req:Request,res:Response, next:NextFunction)=>{
  let wid = req.params.id
  let w = await WocModel.find({_id:wid})
  if (w[0]['uid']==req['user']._id){
    await WocModel.findByIdAndRemove(wid)
    res.status(200)
  }else{
    res.status(401)
  }
  res.end()
})



// WOCS BY KNOWN NUMBER (WocsByKnownNumber)
wocRoutes.post('/WocsByKnownNumber',auth,async (req:Request,res:Response, next:express.NextFunction)=>{
  const lang = req.body.lang
  const knownNumber = req.body.knownNumber
  const uid = req['user']._id
  try{
      let items =  await (await WocModel.find({uid,language:lang, known: knownNumber}))
      res.status(200)
      res.json(items)
      res.end()
  }catch(err){
      res.status(500);
      res.end()
      console.error(err)
  }
})

// wocs to learn /wocsToLearn/
wocRoutes.get('/wocsToLearn/:lang',auth,async(req:Request,res:Response, next:NextFunction)=>{
  let uid = req['user']._id
  let lang = req.params.lang
      try{
        let items =  await (await WocModel.find({uid,language:lang}))
        let wocs = sortItemsToLearn(items)
        console.log(wocs.length);
        
        res.status(200)
        res.json(wocs)
        res.end()
    }catch(err){
        res.status(500);
        res.end()
        console.error(err)
    }
})

function sortItemsToLearn(w:any[]){
  let wocs:any[]=[]

  for (let i=2; i>=0; i--){
    switch (i){
      case 0:{
        let missing = 10-wocs.length
        wocs.push(...addXWocs(w,i,missing))
        break
      }
      case 1:{
        if (w.length>=3) {
          wocs.push(...addXWocs(w,i,3))
        }
        break
      }
      case 2:{
        if (w.length>=2) {
          wocs = wocs.concat(addXWocs(w,i,2))
        }
        break
      }
      default:{
        break
      }
    }
  }
  return wocs
}

function addXWocs(w:any[],k:number,n: number){
  let i=0
  let ws:any[]=[]
  w= w.filter(a => a.known == k)
  let l = w.length
  console.log('n',n,'l',l,'k',k,'w',w);
  
  if ( n>l ) return w
  while (i<n){
    let r=getRandomInt(l)
    
    if (!ws.includes(w[r])){
      ws.push(w[r])
      i++
    }
  }
  return ws
}

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}


export { wocRoutes }

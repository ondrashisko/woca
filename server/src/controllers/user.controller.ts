const express = require('express');
const { check, validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const userRouter = express.Router();
import { auth } from '../middleware/auth';
import { UserDoc, UserModule } from '../models/User.schema';

/**
 * @method - POST
 * @param - /signup
 * @description - User SignUp
 */

userRouter.post(
  '/signup',
  [
    check('username', 'Please Enter a Valid Username').not().isEmpty(),
    check('email', 'Please enter a valid email').isEmail(),
    check('password', 'Please enter a valid password').isLength({
      min: 6,
    }),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array(),
      });
    }

    const { username, email, password } = req.body;
    try {
      let user = await UserModule.findOne({
        email,
      });
      if (user) {
        return res.status(400).json({
          msg: 'User Already Exists',
        });
      }

      user = new UserModule({
        username,
        email,
        password,
      });
      const salt = await bcrypt.genSalt(10);
      user.password = await bcrypt.hash(password, salt);

      await user.save();

      const payload = {
        user: {
          id: user.id,
        },
      };

      jwt.sign(
        payload,
        'randomString',
        {
          expiresIn: 10000,
        },
        (err, token) => {
          if (err) throw err;
          res.status(200).json({
            token,
          });
        }
      );
    } catch (err) {
      console.log(err.message);
      res.status(500).send('Error in Saving');
    }
  }
);

userRouter.post(
  '/login',
  [
    check('email', 'Please enter a valid email').isEmail(),
    check('password', 'Please enter a valid password').isLength({
      min: 6,
    }),
  ],
  async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array(),
      });
    }

    const { email, password } = req.body;
    try {
      let user = await UserModule.findOne({
        email,
      });
      if (!user)
        return res.status(400).json({
          message: 'User Not Exist',
        });

      const isMatch = await bcrypt.compare(password, user.password);
      if (!isMatch)
        return res.status(400).json({
          message: 'Incorrect Password !',
        });

      user.password = '';
      const payload = {
        user: user,
      };

      jwt.sign(
        payload,
        'randomString',
        {
          expiresIn: 518400,
        },
        (err, token) => {
          if (err) throw err;
          res.status(200).json({
            token,
            payload,
          });
        }
      );
    } catch (e) {
      console.error(e);
      res.status(500).json({
        message: 'Server Error',
      });
    }
  }
);

/**
 * @method - POST
 * @description - Get LoggedIn User
 * @param - /user/me
 */

userRouter.get('/me', auth, async (req, res) => {
  try {
    // request.user is getting fetched from Middleware after token authentication
    const user = await UserModule.findById(req.user._id);
    res.json(user);
  } catch (e) {
    res.send({ message: 'Error in Fetching user' });
  }
});

/**
 * @method - PUT
 * @description - adds learning language
 * @param - /user/languages
 */
userRouter.put('/languages', auth, async (req, res) => {
  const langs = req.body;
  const _id = req.user._id;
  try {
    const user = await UserModule.findById(_id);
    user.languages = langs;

    await UserModule.findByIdAndUpdate({ _id }, user);
    res.status(200);
  } catch (err) {
    console.error(err);
    res.status(500).send('error in updating users language');
  }
  res.end();
});

/**
 * @method - PUT
 * @description - change learning language
 * @param - /user/change-language
 */
userRouter.put('/change-language', auth, async (req, res) => {
  const lang = req.body.lang;
  console.log('lang',lang);

  const _id = req.user._id;
  try {
    const user = await UserModule.findById(_id);
    if (lang){
      user.choosenLanguage = lang;
    }else{
      user.choosenLanguage = null
    }
      console.log(user);

    await UserModule.findByIdAndUpdate({ _id }, user);
    res.status(200);
  } catch (err) {
    console.error(err);
    res.status(500).send('error updating USERS chosen language');
  }
  res.end();
});

export { userRouter };

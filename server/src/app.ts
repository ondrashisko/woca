import * as express from 'express'
import * as cors from 'cors'
import * as bodyparser from 'body-parser'
import { requestLoggerMiddleware } from './middleware/requestLoggerMiddleware';
import { NextFunction, Request, Response } from 'express';
import {  wocRoutes } from './controllers/woc.controller';
import  { userRouter } from './controllers/user.controller'


const app = express();
app.use(cors());
app.use(bodyparser.json() )


// middleware
app.use(requestLoggerMiddleware)

// Routes
app.use("/api/wocs",wocRoutes)
app.use("/user", userRouter)


app.get("/",(req,res)=>{
    res.send("wocs express server running")
    res.end()
})

export { app }
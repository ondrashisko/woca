
import * as mongoose from "mongoose";

const UserSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now()
  },
  lang: {
    type: String,
    default: 'cs'
  },
  languages: {
    type: Array
  },
  choosenLanguage:{
      type: String,
    }

});

// export model user with UserSchema

const UserModule = mongoose.model<UserDoc>("User", UserSchema)

export { UserModule }

export interface UserDoc extends mongoose.Document {
  email: string,
  password: string,
  sername: string,
  createdAt: {
    type: Date,
    default: Date
  },
  lang: string,
  languages: string[],
  choosenLanguage: string
}

import * as mongoose from 'mongoose'

const WocSchema = new mongoose.Schema({
    cz: { type: String, required: true },
    tran: { type: String, required: true },
    language: { type: String, required: true},
    known: Number,
    date: { type: Date },
    uid:{ type: String, required: true}
})

const WocModel = mongoose.model('Woc', WocSchema)

export { WocModel }

export interface WocInterface {
    _id?: string,
    cz: string,
    tran: string,
    language: string,
    known: Number,
    date: Date
    uid: String
}

import { app } from './app';
import * as http from 'http';
import  * as mongoose  from 'mongoose'

const PORT = process.env.PORT || 8080;
const server = http.createServer(app);
const MONGO_URL = 'mongodb://127.0.0.1:27017/wocabularies'

app.get("/", (req, res) => {
    res.json({ message: "API Working" });
  });
  
server.listen(PORT);
server.on('listening', async () => {
    console.log('server listening on: ' + PORT);
    mongoose.connect(MONGO_URL, { useUnifiedTopology: true ,useNewUrlParser: true })
    mongoose.set('useFindAndModify',false);
    mongoose.connection.on('open',()=>{
        console.info('connected to database.')
    })
    mongoose.connection.on('error',(err)=>{
        console.error(err)
    })
})

